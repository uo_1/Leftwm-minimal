execute pathogen#infect()
syntax on
filetype plugin indent on
set laststatus=2 
set nu 
set relativenumber
set noswapfile
if !has('gui_running')
  set t_Co=256
endif
let g:lightline = {
      \ 'colorscheme': 'materia',
      \ } 

