## **Dependency**

- pulsemixer : https://github.com/GeorgeFilipkin/pulsemixer 

## **Usage**

The software `pulsemixer` is used for the volume control with the bar. 

## **Credits**

Credits given to [AlphaTechnolog](https://github.com/AlphaTechnolog/void-bspwm) for the `getWinTitle` script

