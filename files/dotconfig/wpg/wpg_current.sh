
#!/bin/bash
# Author : Unique one
#
# Set random wallpaper with wpgtk, pywal and shuffle.
# Run this at startup, i.e. on bspwm:  bash  /path/to/this/script  &
# Arch repositories: python-pywal, coreutils
# AUR : wpgtk
# Dependency: pywal , wpgtk , shuf
# Reference for ideas:
# https://bbs.archlinux.org/viewtopic.php?id=212470
# https://www.reddit.com/r/i3wm/comments/b92l3b/a_simple_bash_script_to_enjoy_a_different/

DIR="$HOME/Wallpapers"


while true; do
  file=$(find $DIR/ -type f -print0 | shuf -z -n 1)
  wpg -s $file  $file
  # 30 mins
  sleep 30m
  # 5 mins
  #sleep 5m 
done


