## Color Scheme 
--- 

Tokyo Night: https://github.com/folke/tokyonight.nvim/blob/main/extras/kitty_tokyonight_night.conf  

## Font 

FiraCode Nerd font:  https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.2/FiraCode.zip  

https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/FiraCode  
