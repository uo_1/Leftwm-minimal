//  _        ___                                      ___ _
// | |      / __)_                                   / __|_)
// | | ____| |__| |_ _ _ _ ____      ____ ___  ____ | |__ _  ____    ____ ___  ____
// | |/ _  )  __)  _) | | |    \    / ___) _ \|  _ \|  __) |/ _  |  / ___) _ \|  _ \
// | ( (/ /| |  | |_| | | | | | |  ( (__| |_| | | | | |  | ( ( | |_| |  | |_| | | | |
// |_|\____)_|   \___)____|_|_|_|   \____)___/|_| |_|_|  |_|\_|| (_)_|   \___/|_| |_|
// A WindowManager for Adventurers                         (____/
// For info about configuration please visit https://github.com/leftwm/leftwm/wiki


#![enable(implicit_some)]
(
    modkey: "Mod4",
    mousekey: "Mod1", // Suppose to be alt key. 
    tags: [
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
    ],
    max_window_width: None,
    layouts: [
	"Dwindle",
        "MainAndVertStack",
        "MainAndHorizontalStack",
        // MainAndDeck,
        // GridHorizontal,
        // EvenHorizontal,
        // EvenVertical,
        "Fibonacci",
        // LeftMain,
        // CenterMain,
        "CenterMainBalanced",
        // CenterMainFluid,
        "Monocle",
        "RightWiderLeftStack",
        "LeftWiderRightStack",
    ],
    layout_mode: Tag, // Tag or Workspace
    insert_behavior: Bottom,
    scratchpad: [],
    window_rules: [],
    disable_current_tag_swap: false,
    disable_tile_drag: false,
    //  Can be Sloppy, ClickTo, or Driven
    focus_behaviour: Sloppy,
    focus_new_windows: true,
    keybind: [
        (command: Execute, value: "rofi -show drun -theme ~/.config/rofi/launcher.rasi", modifier: ["modkey"], key: "r"),
        
        // Terminal Emulator  
        (command: Execute, value: "kitty", modifier: ["modkey"], key: "Return"),
        (command: Execute, value: "wezterm", modifier: ["Control", "Alt"], key: "t"),
         
        (command: Execute, value: "loginctl kill-session $XDG_SESSION_ID", modifier: ["modkey", "Shift"], key: "x"),
	
	// Applications
	(command: Execute, value: "geany", modifier: ["modkey"], key: "e"),
        (command: Execute, value: "thunar", modifier: ["modkey", "Shift"], key: "f"),

	// Layout Switcher  credits goes to di-effe[https://github.com/di-effe/catppuccin]
	(command: Execute, value: "$HOME/.config/eww/sidebar-leftwm/scripts/layout.sh", modifier: ["modkey", "Shift"], key: "l"),

	// Keybinds ShortKey
	(command: Execute, value: "rofi -dmenu -input /tmp/leftwm-keymap -window-title 'Keymap' -theme ~/.config/rofi/keymap.rasi", modifier: ["Alt"], key: "Escape"),

	// Hide/UnHide Windows
	(command: Execute, value: "$HOME/.config/leftwm/scripts/winmask", modifier: ["modkey"], key: "d"),


	
        (command: CloseWindow, value: "", modifier: ["Alt"], key: "c"),
        (command: Execute, value: "pkill leftwm-worker", modifier: ["modkey", "Control"], key: "q"),
        (command: SoftReload, value: "", modifier: ["modkey", "Control"], key: "r"),
        // (command: Execute, value: "slock", modifier: ["modkey", "Control"], key: "l"),

        (command: MoveToLastWorkspace, value: "", modifier: ["modkey", "Shift"], key: "w"),
        (command: SwapTags, value: "", modifier: ["modkey"], key: "Tab"),
        (command: MoveWindowUp, value: "", modifier: ["modkey", "Shift"], key: "k"),
        (command: MoveWindowDown, value: "", modifier: ["modkey", "Shift"], key: "j"),
        (command: MoveWindowTop, value: "", modifier: ["modkey", "Shift"], key: "Return"),
        (command: FocusWindowUp, value: "", modifier: ["modkey"], key: "k"),
        (command: FocusWindowDown, value: "", modifier: ["modkey"], key: "j"),
        (command: NextLayout, value: "", modifier: ["modkey", "Control"], key: "k"),
        (command: PreviousLayout, value: "", modifier: ["modkey", "Control"], key: "j"),
        (command: FocusWorkspaceNext, value: "", modifier: ["modkey"], key: "l"),
        (command: FocusWorkspacePrevious, value: "", modifier: ["modkey"], key: "h"),
        (command: MoveWindowUp, value: "", modifier: ["modkey", "Shift"], key: "Up"),
        (command: MoveWindowDown, value: "", modifier: ["modkey", "Shift"], key: "Down"),
        (command: FocusWindowUp, value: "", modifier: ["modkey"], key: "Up"),
        (command: FocusWindowDown, value: "", modifier: ["modkey"], key: "Down"),


        (command: NextLayout, value: "", modifier: ["modkey", "Control"], key: "Up"),
        (command: PreviousLayout, value: "", modifier: ["modkey", "Control"], key: "Down"),
        (command: FocusWorkspaceNext, value: "", modifier: ["modkey"], key: "Right"),
        (command: FocusWorkspacePrevious, value: "", modifier: ["modkey"], key: "Left"),

        // Focus next and previous tags
        (command: FocusNextTag, value: "", modifier: ["modkey"], key: "w"),
        (command: FocusPreviousTag, value: "", modifier: ["modkey"], key : "s"),


        // Set Layouts (Floating , Fullscreen, Monocle, etc.)
        (command: ToggleFloating, value: "", modifier: ["modkey"], key: "space"),
        (command: ToggleFullScreen, value: "", modifier: ["modkey"], key: "f"),
        (command: SetLayout, value: "Monocle", modifier: ["Alt"], key: "m"),
        (command: SetLayout, value: "MainAndVertStack", modifier: ["modkey"], key: "t"),
        (command: SetLayout, value: "LeftWiderRightStack", modifier: ["modkey", "Shift"], key: "t"),

        // Resize Windows
        (command: IncreaseMainWidth, value: "5", modifier: ["modkey"], key: "bracketright"),
        (command: DecreaseMainWidth, value: "5", modifier: ["modkey"], key: "bracketleft"),

 	// Resize Windows with your mouse (Beta Functionality)
	(command: IncreaseMainWidth, value: "5", modifier: ["modkey"], key: "Pointer_Drag1"),
        (command: DecreaseMainWidth, value: "5", modifier: ["modkey"], key: "Pointer_Drag3"),



        // System Keys
        // Volume Control
        
	// Decrease Volume
        (command: Execute, value: "amixer set Master 5%-", modifier: [], key: "XF86XK_AudioLowerVolume"),
	
	// Increase Volume
        (command: Execute, value: "amixer set Master 5%+", modifier: [], key: "XF86XK_AudioRaiseVolume"),
	
	// Mute Volume
        (command: Execute, value: "amixer set Master toggle", modifier: [], key: "XF86XK_AudioMute"),



        (command: GotoTag, value: "1", modifier: ["modkey"], key: "1"),
        (command: GotoTag, value: "2", modifier: ["modkey"], key: "2"),
        (command: GotoTag, value: "3", modifier: ["modkey"], key: "3"),
        (command: GotoTag, value: "4", modifier: ["modkey"], key: "4"),
        (command: GotoTag, value: "5", modifier: ["modkey"], key: "5"),
        (command: GotoTag, value: "6", modifier: ["modkey"], key: "6"),
        (command: GotoTag, value: "7", modifier: ["modkey"], key: "7"),
        (command: GotoTag, value: "8", modifier: ["modkey"], key: "8"),
        (command: MoveToTag, value: "1", modifier: ["modkey", "Shift"], key: "1"),
        (command: MoveToTag, value: "2", modifier: ["modkey", "Shift"], key: "2"),
        (command: MoveToTag, value: "3", modifier: ["modkey", "Shift"], key: "3"),
        (command: MoveToTag, value: "4", modifier: ["modkey", "Shift"], key: "4"),
        (command: MoveToTag, value: "5", modifier: ["modkey", "Shift"], key: "5"),
        (command: MoveToTag, value: "6", modifier: ["modkey", "Shift"], key: "6"),
        (command: MoveToTag, value: "7", modifier: ["modkey", "Shift"], key: "7"),
        (command: MoveToTag, value: "8", modifier: ["modkey", "Shift"], key: "8"),
    ],
    workspaces: [],
)
