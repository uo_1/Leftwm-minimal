#!/usr/bin/bash





# Start Other Applications
$HOME/.config/leftwm/scripts/clean  &


# Keyboard layout
setxkbmap -layout us &

# Display settings
$HOME/.screenlayout/Display.sh &

# Compositor
picom --config "$HOME/.config/picom/picom.conf" -b  &


# Set Wallpaper 
"$HOME/.config/wpg/wpg_current.sh"  &


# Network Manager Utility 
if [[ -x "$(command -v nm-applet)" && -f ]]; then
  killall -q nm-applet; nm-applet  &
fi



# Notification daemon
killall -q wired; wired &

# Anti sleep
xset s off &
xset -dpms &





# EWW widgets
eww  --config  "$HOME/.config/eww/sidebar-leftwm/" kill 
eww  --config  "$HOME/.config/eww/sidebar-leftwm/" daemon 
eww  --config  "$HOME/.config/eww/sidebar-leftwm/" open bar 

# System Tray
if [[ -x "$(command -v stalonetray)" && -f "$HOME/.config/stalonetray/.stalonetrayrc" ]]; then
   killall -q stalonetray; stalonetray --config "$HOME/.config/stalonetray/.stalonetrayrc" &
fi


# Keyring
eval $(gnome-keyring-daemon --start)
export SSH_AUTH_SOCK

