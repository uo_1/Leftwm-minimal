# Leftwm Dotfiles

## Preview

<details>
    <summary><strong>Leftwm Minimal</strong></summary>

![Leftwm Minimal](/files/asset/Desktop_arch-testbed.png)

</details>

**Dependencies**

- GTK Theme : [Rose-Pine-GTK-Theme](https://github.com/Fausto-Korpsvart/Rose-Pine-GTK-Theme)

- Cursor : [Phinger cursors](https://github.com/phisch/phinger-cursors)

- Icons Theme : [RosePine Icons](https://github.com/rose-pine/gtk)

- Fonts : 

    - [Inter](https://github.com/rsms/inter)

    - [Jetbrains Mono](https://github.com/JetBrains/JetBrainsMono)

    - [Font Awesome](https://fontawesome.com/download)

    - [tabler-icons](https://github.com/tabler/tabler-icons)

    - [Comic Mono](https://dtinth.github.io/comic-mono-font/)

    - [Iosevka Nerd Font](https://www.nerdfonts.com/)

- Terminal : [kitty](https://github.com/kovidgoyal/kitty)

- Launcher : [rofi](https://github.com/davatorium/rofi)

- Bar/Panel : [eww](https://github.com/elkowar/eww)

- System tray : [stalonetray](https://github.com/kolbusa/stalonetray/)

- Notifications : [wired-notify](https://github.com/Toqozz/wired-notify)

- Color Picker : [gpick](https://github.com/thezbyg/gpick)

- System monitor : [gotop](https://github.com/xxxserxxx/gotop) ,  [bottom](https://github.com/ClementTsang/bottom)

- Compositor : [picom](https://github.com/yshui/picom)

- ColorScheme Generator : 
    
    - [pywal](https://github.com/dylanaraps/pywal)

    - [wpgtk](https://github.com/deviantfero/wpgtk)

- Wallpapers : [wallpapers.md](files/wallpaper.md)

## Install

Install the dependencies on your system first.

Other Dependencies


- git

- rsync



Install the [Window Manager : Leftwm](https://github.com/leftwm/leftwm)

## Usage

```

    $ git clone -b arch-linux  https://gitlab.com/uo_1/Leftwm-minimal.git

    $ cd Leftwm-minimal

    $ rsync -auLP   files/dotconfig/*     ~/.config/

    $ mkdir  ~/Wallpapers

    $ cp -v Images/*    ~/Wallpapers

    $ cp -rv  .xprofile    .xinitrc   .vimrc    .fehbg    .Xdracula    ~/

```

## Credits 

[Credits](credits/URL.md)
